package info.iffood;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class viewHolder extends RecyclerView.ViewHolder {

    ImageView buttonmais;
    ImageView buttonmenos;
    TextView title;
    TextView price;
    TextView numeroTrufas;
    ImageView imagem;
    ProgressBar loadingImage;

    viewHolder(View itemView) {
        super(itemView);

        buttonmais = itemView.findViewById(R.id.btnMais);
        buttonmenos = itemView.findViewById(R.id.btnMenos);
        title = itemView.findViewById(R.id.title);
        price = itemView.findViewById(R.id.price);
        imagem = itemView.findViewById(R.id.imagem);
        numeroTrufas = itemView.findViewById(R.id.ntrufas);
        loadingImage = itemView.findViewById(R.id.loadingImage);

    }

}
