package info.iffood;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {
    private EditText CaixaDeTexto;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loginactivity_main);


        //ESTILO DE FONTE TELA INICIAL (TELA DE LOGIN)
        CaixaDeTexto = findViewById(R.id.caixatextologin);
        Button botLogin = findViewById(R.id.botlog);
        //----------------------------------------------------------------------------------------------
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor = sharedPreferences.edit();

        //Verifica se o usuario ja logou se sim,vai pra main activity, E Envia O Usuario Para Main Activity

        if (sharedPreferences.contains("verificarlogin")) {
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
        }
        //----------------------------------------------------------------------------------------------

        //AO ABRIR A CAIXA DE TEXTO O ENTER AGORA FUNCIONA COMO UM 'OK'
        CaixaDeTexto.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    assert imm != null;
                    imm.hideSoftInputFromWindow(CaixaDeTexto.getWindowToken(), 0);
                }
                return false;
            }
        });
        //---------------------------------------------------------------------------------------------
        //VERIFICA SE FOI DIGITADO ALGUMA COISA NO NOME------------------------------------------------
        botLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nome = CaixaDeTexto.getText().toString();
                if (nome.isEmpty()) {
                    Toast.makeText(LoginActivity.this, "Digite seu nome na caixa de texto.", Toast.LENGTH_LONG).show();
                } else {
                    if(nome.trim().length()==0){
                        Toast.makeText(LoginActivity.this, "Por favor, não coloque apenas espaço no seu nome.", Toast.LENGTH_LONG).show();
                    }else {
                        //Verifica o login do Usuario-------------------------------------------------------------------
                        editor.putBoolean("verificarlogin", true);
                        editor.putString("nome_usuario", nome);
                        editor.apply();
                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    }
                }
            }
        });

        //----------------------------------------------------------------------------------------------


    }


}

