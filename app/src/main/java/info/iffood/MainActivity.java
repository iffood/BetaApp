package info.iffood;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends ComprarActivity {

    public boolean notificar = false;
    private static final String URLVendedores = "http://iffood.info/_core/_controller/vendedores.php";
    private static final String URLProdutos = "http://iffood.info/_core/_controller/produtos.php";
    private String parametros = "";
    private Handler h = new Handler();
    private int delay = 10000; //1 second=1000 milisecond, 15000=15seconds
    private Runnable runnable;
    private ArrayList<String> id = new ArrayList<>();
    private ArrayList<String> nome = new ArrayList<>();
    private ArrayList<String> precos = new ArrayList<>();
    private ArrayList<String> img = new ArrayList<>();
    private ArrayList<Source> list = new ArrayList<>();
    private TextView ValorTotal;
    private SharedPreferences mPreferences;
    private SharedPreferences.Editor mEditor;
    private int numeroVendedoresOnline;
    private RelativeLayout snackLayout;
    //private boolean checkPreferences = false;
    private String statusPedido;

    @SuppressLint("CommitPrefEdits")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new Thread(new Runnable() {
            @Override
            public void run() {
                MainActivity.this.runOnUiThread(new Runnable() {
                    @SuppressLint("ClickableViewAccessibility")
                    @Override
                    public void run() {

                        getProdutos();

                    }
                });

            }
        }).start();

        snackLayout = findViewById(R.id.snackLayout);

        mPreferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        mEditor = mPreferences.edit();

        statusPedido = mPreferences.getString("status", "Não recebido");

        /*new Thread(new Runnable() {
            @Override
            public void run() {
                SharedPreferences mPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                SharedPreferences.Editor editor = mPreferences.edit();
                editor.remove("valor_total").apply();
                editor.remove("numero_trufas").apply();

                mPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                String idsMain = mPreferences.getString("idMain", "Não recebido");
                String[] itemIds = idsMain.split(",");
                Collections.addAll(id, itemIds);

                mPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                String nomeMain = mPreferences.getString("nomeMain", "Não recebido");
                String[] itemNomes = nomeMain.split(",");
                Collections.addAll(nome, itemNomes);

                mPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                String precoMain = mPreferences.getString("preçoMain", "Não recebido");
                String[] itemsMain = precoMain.split(",");
                Collections.addAll(precos, itemsMain);

                mPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                String imgMain = mPreferences.getString("imgMain", "Não recebido");
                String[] itemImgs = imgMain.split(",");
                Collections.addAll(img, itemImgs);

                Log.i("ResposShared", id.toString());
                Log.i("ResposShared", nome.toString());
                Log.i("ResposShared", precos.toString());
                Log.i("ResposShared", img.toString());

                for (int i = 0; i < id.size(); i++) {
                    if (id.get(i).equals("Não recebido") || id.get(i).equals("")) {
                        checkPreferences = true;
                    }
                }

                for (int i = 0; i < nome.size(); i++) {
                    if (nome.get(i).equals("Não recebido") || nome.get(i).equals("")) {
                        checkPreferences = true;
                    }
                }

                for (int i = 0; i < precos.size(); i++) {
                    if (precos.get(i).equals("Não recebido") || precos.get(i).equals("")) {
                        checkPreferences = true;
                    }
                }

                for (int i = 0; i < img.size(); i++) {
                    if (img.get(i).equals("Não recebido") || img.get(i).equals("")) {
                        checkPreferences = true;
                    }
                }

                Log.i("Check", String.valueOf(checkPreferences));

                if (!checkPreferences) {
                    try {
                        for (int i = 0; i < id.size(); i++) {
                            list.add(new Source(nome.get(i), "R$" + precos.get(i), img.get(i)));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    getProdutos();

                    final RecyclerView container = findViewById(R.id.container);
                    container.setHasFixedSize(true);
                    final LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                    layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                    Adapter adapter = new Adapter(list, getApplicationContext(), snackLayout);
                    container.setAdapter(adapter);
                    container.setLayoutManager(layoutManager);

                }

            }
        }).start();*/


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setLogo(R.drawable.iffood_launcher);

        //----------------------------------------------------------
        ValorTotal = findViewById(R.id.ValorTotal);

        Thread t = new Thread() {

            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(100);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                SharedPreferences mPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                                @SuppressLint("CommitPrefEdits") SharedPreferences.Editor editor = mPreferences.edit();
                                float vtot = mPreferences.getFloat("valor_total", 0);
                                int ntrufas = mPreferences.getInt("numero_trufas", 0);
                                ValorTotal.setText("Valor total: R$" + String.valueOf(vtot) + "0");
                            }
                        });
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        t.start();

        // ---------------------------------------------------------


        // ---------------------------------------------------------
        /*BottomNavigationView mBottomNavigationView = findViewById(R.id.bottomNavigation);
        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) mBottomNavigationView.getLayoutParams();
        layoutParams.setBehavior(new BottomNavigationViewBehavior());
        mBottomNavigationView.setOnNavigationItemSelectedListener(new OnNavigationItemSelectedListener() {
            @SuppressLint("CommitPrefEdits")
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.cardapio:
                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

                        break;
                    case R.id.cart:

                        ConnectivityManager connectivityManager = (ConnectivityManager)
                                getSystemService(Context.CONNECTIVITY_SERVICE);
                        assert connectivityManager != null;
                        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

                        if (networkInfo != null && networkInfo.isConnected()) {

                            mPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                            mEditor = mPreferences.edit();

                            statusPedido = mPreferences.getString("status", "Não recebido");

                            int numeroTrufas = mPreferences.getInt("numero_trufas", 0);
                            //RETIRAR COMENTARIOS PARA IMPEDIR QUE O USUARIO PROSSIGA CASO NÃO TENHA VENDEDORES ONLINE
                            if (numeroTrufas != 0) {
                                if (numeroVendedoresOnline != 0) {
                                    if (statusPedido.equals("aguardando") || statusPedido.equals("aceitado")) {
                                        Snackbar snackbar = Snackbar.make(snackLayout, "Você tem um pedido em andamento!", Snackbar.LENGTH_LONG)
                                                .setAction("Meu pedido", new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        Intent intent = new Intent(MainActivity.this, InformacoesPedidoActivity.class);
                                                        startActivity(intent);
                                                    }
                                                });
                                        snackbar.show();
                                    } else {
                                        startActivity(new Intent(getApplicationContext(), ComprarActivity.class));
                                        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                                    }
                                } else {
                                 abrirDialog();
                                }
                            } else {
                                Snackbar snackbar = Snackbar.make(snackLayout, "Selecione algum produto!", Snackbar.LENGTH_LONG);
                                snackbar.show();
                            }

                        } else {
                            Snackbar snackbar = Snackbar.make(snackLayout, "Nenhuma conexão detectada", Snackbar.LENGTH_LONG);
                            snackbar.show();
                        }

                }
                return false;
            }
        });*/
        //-----------------------------------------------------------
    }


    private void launchMarket() {
        Uri uri = Uri.parse("market://details?id=" + getPackageName());
        Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
        try {
            startActivity(myAppLinkToMarket);
        } catch (ActivityNotFoundException e) {
            Snackbar snackbar = Snackbar.make(snackLayout, "Não foi possível encontrar app na PlayStore!", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.configuracoes:
                //ABRIR ACTIVITY DE CONTATO
                startActivity(new Intent(getApplicationContext(), configuracaoActivity.class));
                break;
            case R.id.resetarValores:
                mPreferences = PreferenceManager.getDefaultSharedPreferences(this);
                mEditor = mPreferences.edit();
                mEditor.remove("valor_total").apply();
                Intent intent = new Intent(MainActivity.this, MainActivity.class);
                this.finish();
                startActivity(intent);
                break;
            case R.id.sobrenosid:
                startActivity(new Intent(MainActivity.this, MainActivity_SobreNos.class));
                break;
            case R.id.meuspedidos:
                mPreferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                String observation = mPreferences.getString("observation", "Não recebido");
                if (statusPedido.equals("aguardando") || statusPedido.equals("aceitado")) {
                    startActivity(new Intent(MainActivity.this, InformacoesPedidoActivity.class));
                } else {
                    if (statusPedido.equals("recusado")) {
                        dialogRecusado(observation);
                        mEditor = mPreferences.edit();
                        mEditor.remove("status").apply();
                    }
                    Snackbar snackbar = Snackbar.make(snackLayout, "Nenhum pedido pendente!", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
                break;
            case R.id.avaliar:
                seguirPlayStore();
                break;
            case R.id.comprar:
                ConnectivityManager connectivityManager = (ConnectivityManager)
                        getSystemService(Context.CONNECTIVITY_SERVICE);
                assert connectivityManager != null;
                NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

                if (networkInfo != null && networkInfo.isConnected()) {

                    mPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    mEditor = mPreferences.edit();

                    statusPedido = mPreferences.getString("status", "Não recebido");

                    int numeroTrufas = mPreferences.getInt("numero_trufas", 0);
                    //RETIRAR COMENTARIOS PARA IMPEDIR QUE O USUARIO PROSSIGA CASO NÃO TENHA VENDEDORES ONLINE
                    if (numeroTrufas != 0) {
                        if (numeroVendedoresOnline != 0) {
                            if (statusPedido.equals("aguardando") || statusPedido.equals("aceitado")) {
                                Snackbar snackbar = Snackbar.make(snackLayout, "Você tem um pedido em andamento!", Snackbar.LENGTH_LONG)
                                        .setAction("Meu pedido", new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                Intent intent = new Intent(MainActivity.this, InformacoesPedidoActivity.class);
                                                startActivity(intent);
                                            }
                                        });
                                snackbar.show();
                            } else {
                                startActivity(new Intent(getApplicationContext(), ComprarActivity.class));
                                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                            }
                        } else {
                            abrirDialog();
                        }
                    } else {
                        Snackbar snackbar = Snackbar.make(snackLayout, "Selecione algum produto!", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }

                } else {
                    Snackbar snackbar = Snackbar.make(snackLayout, "Nenhuma conexão detectada", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            default:
        }
        return true;
    }

    private void dialogRecusado(String observation) {

        android.app.AlertDialog.Builder alerta = new android.app.AlertDialog.Builder(MainActivity.this);
        alerta.setTitle("Pedido Rejeitado :(");
        alerta.setIcon(R.drawable.ic_check_alertdialog_web);
        alerta.setMessage("Infelizmente o vendedor teve que recusar seu pedido. Tente fazer outro. \nMotivo da recusa: " + observation);
        alerta.setCancelable(false);
        alerta.setPositiveButton("Ok :(", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        android.app.AlertDialog AlertDialog = alerta.create();
        AlertDialog.show();

    }

    private void verificarInternet(){

        ConnectivityManager connectivityManager = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {

            parametros = "action=" + "getProdutos";

            new MainActivity.solicitaDados().execute(URLProdutos);
        } else {
            //Toast.makeText(this, "Nenhuma conexão com internet foi detectada! Habilite a internet!", Toast.LENGTH_SHORT).show();
            Snackbar snackbar = Snackbar.make(snackLayout, "Nenhuma conexão detectada", Snackbar.LENGTH_LONG);
            snackbar.show();
        }


    }

    @SuppressLint("CommitPrefEdits")
    @Override
    protected void onStart() {

        mPreferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        mEditor = mPreferences.edit();

        statusPedido = mPreferences.getString("status", "Não recebido");

        getVendedores();
        super.onStart();
    }

    @SuppressLint("CommitPrefEdits")
    @Override
    protected void onPause() {


        h.postDelayed(new Runnable() {
            @SuppressLint("CommitPrefEdits")
            @Override
            public void run() {

                getVendedores();
                //Toast.makeText(MainActivity.this, "Verificado", Toast.LENGTH_SHORT).show();

                mPreferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                mEditor = mPreferences.edit();

                String statusPassado = statusPedido;
                statusPedido = mPreferences.getString("status", "Não recebido");

                Log.i("Status passado", statusPassado);
                Log.i("Status atual", statusPedido);

                if (!statusPassado.equals(statusPedido) && !statusPassado.equals("Não recebido")) {
                    notificarNovoStatus();
                }

                runnable = this;
                h.postDelayed(runnable, delay);
            }
        }, delay);

        super.onPause();

    }

    @Override
    protected void onStop() {
        h.removeCallbacks(runnable);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        h.removeCallbacks(runnable);
        super.onDestroy();
    }

    private void getVendedores() {

        ConnectivityManager connectivityManager = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {

            parametros = "action=" + "getOnVendedores";

            new MainActivity.solicitaDados().execute(URLVendedores);
        } else {
            //Toast.makeText(this, "Nenhuma conexão com internet foi detectada! Habilite a internet!", Toast.LENGTH_SHORT).show();
            Snackbar snackbar = Snackbar.make(snackLayout, "Nenhuma conexão detectada", Snackbar.LENGTH_LONG);
            snackbar.show();
        }

    }

    @Override
    protected void onResume() {

        h.postDelayed(new Runnable() {
            @SuppressLint("CommitPrefEdits")
            @Override
            public void run() {

                if(id.isEmpty()) {
                    verificarInternet();

                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                getVendedores();
                //Toast.makeText(MainActivity.this, "Verificado", Toast.LENGTH_SHORT).show();

                mPreferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                mEditor = mPreferences.edit();

                String statusPassado = statusPedido;
                statusPedido = mPreferences.getString("status", "Não recebido");

                Log.i("Status passado", statusPassado);
                Log.i("Status atual", statusPedido);

                if (!statusPassado.equals(statusPedido) && !statusPassado.equals("Não recebido")) {
                    notificarNovoStatus();
                }

                runnable = this;
                h.postDelayed(runnable, delay);
            }
        }, delay);

        super.onResume();
    }

    private void getProdutos() {

        ConnectivityManager connectivityManager = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {

            parametros = "action=" + "getProdutos";

            new MainActivity.solicitaDados().execute(URLProdutos);
        } else {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Nenhuma conexão detectada", Snackbar.LENGTH_LONG);
            snackbar.show();
        }

    }

    private void notificarNovoStatus() {

        Intent intent = new Intent(this, InformacoesPedidoActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, 0);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.logo_iffood)
                .setAutoCancel(true)
                .setColor(getResources().getColor(R.color.colorAccent))
                .setContentIntent(pendingIntent)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.if_ic))
                .setContentTitle("IFFOOD")
                .setContentText("Status do seu pedido: " + statusPedido);
        notificationBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(MainActivity.this);
        notificationManager.notify(1, notificationBuilder.build());
    }

    private void sendNotification() {
        //NOTIFICAÇÃO CASO TENHA MAIS DE 5 VENDEDORES ONLINE
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, 0);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.logo_iffood)
                .setAutoCancel(true)
                .setColor(getResources().getColor(R.color.colorAccent))
                .setContentIntent(pendingIntent)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.if_ic))
                .setContentTitle("IFFOOD")
                .setContentText(numeroVendedoresOnline + " vendedores online, faça seu pedido!");
        notificationBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(MainActivity.this);
        notificationManager.notify(1, notificationBuilder.build());

    }

    private void avisarNotifications() {
        new AlertDialog.Builder(this)
                .setTitle("Aviso!")
                .setMessage("Esta função está em desenvolvimento e só irá funcionar se o aplicativo estiver em segundo plano.")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
    }

    private void seguirPlayStore() {
        new AlertDialog.Builder(this)
                .setTitle("")
                .setMessage("Deseja avaliar este aplicativo?")
                .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ConnectivityManager connectivityManager = (ConnectivityManager)
                                getSystemService(Context.CONNECTIVITY_SERVICE);
                        assert connectivityManager != null;
                        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

                        if (networkInfo != null && networkInfo.isConnected()) {
                            launchMarket();
                        } else {
                            Snackbar snackbar = Snackbar.make(snackLayout, "Nenhuma conexão detectada! Habilite a internet e tente novamente!", Snackbar.LENGTH_LONG);
                            snackbar.show();
                        }

                    }
                })
                .setNegativeButton("Não", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
    }

    private void abrirDialog() {
        new AlertDialog.Builder(this)
                .setTitle("Nenhum vendedor disponível no momento :(")
                .setMessage("Deseja receber notificações para saber quando há vendedores online?")
                .setPositiveButton("Receber notificações", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Snackbar snackbar = Snackbar.make(snackLayout, "Notificações serão enviadas!", Snackbar.LENGTH_LONG);
                        snackbar.show();
                        avisarNotifications();
                        notificar = true;
                    }
                })
                .setNegativeButton("Não", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Snackbar snackbar = Snackbar.make(snackLayout, "Notificações não serão enviadas :(", Snackbar.LENGTH_LONG);
                        snackbar.show();
                        notificar = false;
                    }
                })
                .show();
    }

    private void initRecyclerView() {

        try {
            for (int i = 0; i < id.size(); i++) {
                list.add(new Source(nome.get(i), "R$" + precos.get(i), img.get(i)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        Adapter adapter = new Adapter(list, MainActivity.this, snackLayout);
        final RecyclerView container = findViewById(R.id.container);
        container.setHasFixedSize(true);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        container.setItemViewCacheSize(6);
        container.setDrawingCacheEnabled(true);
        container.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        container.setLayoutManager(layoutManager);
        container.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        ProgressBar progressBar = findViewById(R.id.pregress_bar);
        progressBar.setVisibility(View.GONE);

    }

    @SuppressLint("StaticFieldLeak")
    private class solicitaDados extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {
            return Connection.postDados(urls[0], parametros);
        }

        @Override
        protected void onPostExecute(String resultado) {

            //parsingVendedoresJSON(resultado);
            //Toast.makeText(MainActivity.this, parsingVendedoresJSON(resultado).toString(), Toast.LENGTH_SHORT).show();
            //PEGAR VALORES DOS PRODUTOS

            if (resultado != null && resultado.contains("id")) {
                Log.i("Respostinha", resultado);

                id.clear();
                nome.clear();
                precos.clear();
                img.clear();
                list.clear();

                mPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                mEditor = mPreferences.edit();
                mEditor.remove("valor_total").apply();

                try {
                    JSONArray pedidos = new JSONArray(resultado);
                    for (int i = 0; i < pedidos.length(); i++) {
                        JSONObject pedido = pedidos.getJSONObject(i);
                        id.add(pedido.get("id").toString());
                        nome.add(pedido.get("nome").toString());
                        precos.add(pedido.get("preco").toString());
                        img.add(pedido.get("img").toString());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //Log.i("Respos", id.toString());
                //Log.i("Respos", nome.toString());
                //Log.i("Respos", precos.toString());
                //Log.i("Respos", img.toString());

               /* if (id.isEmpty() || nome.isEmpty() || precos.isEmpty() || img.isEmpty()) {
                    getProdutos();
                    initRecyclerView();
                }*/

                initRecyclerView();

                /*if (list.isEmpty()) {
                    getProdutos();
                    initRecyclerView();
                }*/

                /*new Thread(new Runnable() {
                    @Override
                    public void run() {

                        StringBuilder stringBuilderIds = new StringBuilder();
                        for (String s : id) {
                            stringBuilderIds.append(s);
                            stringBuilderIds.append(",");
                        }

                        StringBuilder stringBuilderNomes = new StringBuilder();
                        for (String s : nome) {
                            stringBuilderNomes.append(s);
                            stringBuilderNomes.append(",");
                        }

                        StringBuilder stringBuilderprecos = new StringBuilder();
                        for (String s : precos) {
                            stringBuilderprecos.append(s);
                            stringBuilderprecos.append(",");
                        }

                        StringBuilder stringBuilderImgs = new StringBuilder();
                        for (String s : img) {
                            stringBuilderImgs.append(s);
                            stringBuilderImgs.append(",");
                        }

                        mPreferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                        mEditor = mPreferences.edit();

                        mEditor.putString("idMain", stringBuilderIds.toString());
                        mEditor.putString("nomeMain", stringBuilderNomes.toString());
                        mEditor.putString("preçoMain", stringBuilderprecos.toString());
                        mEditor.putString("imgMain", stringBuilderImgs.toString());

                        mEditor.apply();

                        try {
                            for (int i = 0; i < id.size(); i++) {
                                list.add(new Source(nome.get(i), "R$" + precos.get(i), img.get(i)));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        Log.i("Respos", id.toString());
                        Log.i("Respos", nome.toString());
                        Log.i("Respos", precos.toString());
                        Log.i("Respos", img.toString());
                    }
                }).start();*/

            }

            try {
                if (resultado != null && !resultado.equals("") && !resultado.contains("nome")) {
                    Log.i("QUE", resultado);
                    numeroVendedoresOnline = Integer.parseInt(resultado);
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }


            Log.i("Vendedores", String.valueOf(numeroVendedoresOnline));

            //NOTIFICAÇÃO

            if (numeroVendedoresOnline > 0 && notificar) {
                sendNotification();
                notificar = false;

            }

        }
    }

}
