package info.iffood;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.net.URLEncoder;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class SobreNosAdapter extends RecyclerView.Adapter<SobreNosAdapter.SobreNosViewHolder> {

    private Context mContext;
    private ArrayList<IffoodTeam> teamList;

    SobreNosAdapter(Context mContext, ArrayList<IffoodTeam> teamList) {
        this.mContext = mContext;
        this.teamList = teamList;
    }

    @Override
    public SobreNosViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.sobrenos_card, parent, false);
        view.setMinimumWidth(parent.getMeasuredWidth());
        return new SobreNosViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SobreNosViewHolder holder, int position) {

        holder.nome.setText(teamList.get(position).getNome());
        holder.idade.setText(teamList.get(position).getIdade());
        holder.frase.setText(teamList.get(position).getFrase());
        holder.background.setImageDrawable(mContext.getResources().getDrawable(teamList.get(position).getBackground()));
        holder.foto.setImageDrawable(mContext.getResources().getDrawable(teamList.get(position).getFoto()));
        holder.face.setImageDrawable(mContext.getResources().getDrawable(R.drawable.facebook));
        holder.linkedin.setImageDrawable(mContext.getResources().getDrawable(R.drawable.linkedin));
        holder.email.setImageDrawable(mContext.getResources().getDrawable(R.drawable.email));
        holder.whatsapp.setImageDrawable(mContext.getResources().getDrawable(R.drawable.whatsapp));

        holder.whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int position = holder.getAdapterPosition();
                switch (teamList.get(position).getNome()) {
                    case "Gabriel Bastos":
                        //dialogWhatsApp("(65) 99316-9240");
                        openWhatsappContact("+55 (65) 99316-9240");
                        break;
                    case "Felipe Jung":
                        //dialogWhatsApp("(66) 99963-4863");
                        openWhatsappContact("+55 (66) 99963-4863");
                        break;
                    case "Luis Eduardo":
                        //dialogWhatsApp("(65) 9688-7210");
                        openWhatsappContact("+55 (65) 9688-7210");
                        break;
                    case "Tiago Relembrando":
                        //dialogWhatsApp("(65) 9930-0816");
                        openWhatsappContact("+55 (65) 9930-0816");
                        break;
                    case "Naíra de Moura":
                        //dialogWhatsApp("(65) 9651-6173");
                        openWhatsappContact("+55 (65) 9651-6173");
                        break;
                    case "Lucas Eduardo":
                        //dialogWhatsApp("(65) 9914-9008");
                        openWhatsappContact("+55 (65) 9914-9008");
                    default:
                }
            }
        });

        holder.face.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = holder.getAdapterPosition();
                switch (teamList.get(position).getNome()) {
                    case "Gabriel Bastos":
                        openFacebook("https://www.facebook.com/profile.php?id=100007777650593");
                        break;
                    case "Felipe Jung":
                        openFacebook("https://www.facebook.com/profile.php?id=100007746364482");
                        break;
                    case "Luis Eduardo":
                        openFacebook("https://www.facebook.com/profile.php?id=100002490514242");
                        break;
                    case "Tiago Relembrando":
                        openFacebook("https://www.facebook.com/profile.php?id=100003873166528");
                        break;
                    case "Naíra de Moura":
                        openFacebook("https://www.facebook.com/profile.php?id=100007310610726");
                        break;
                    case "Lucas Eduardo":
                        openFacebook("https://www.facebook.com/profile.php?id=100005288627219");
                    default:
                }
            }
        });

        holder.linkedin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = holder.getAdapterPosition();
                switch (teamList.get(position).getNome()) {
                    case "Gabriel Bastos":
                        openLinkedin("https://www.linkedin.com/in/gabriel-bastos-26b53314a/");
                        break;
                    case "Felipe Jung":
                        openLinkedin("https://www.linkedin.com/in/felipe-jung-01531a160/");
                        break;
                    case "Luis Eduardo":
                        openLinkedin("https://www.linkedin.com/in/luis-eduardo-souza-95187814a/");
                        break;
                    case "Tiago Relembrando":
                        openLinkedin("https://www.linkedin.com/in/tiago-hellebrandt-silva/");
                        break;
                    case "Naíra de Moura":
                        openLinkedin("https://www.linkedin.com/in/na%C3%ADra-moura-e-souza-3a07b5162/");
                        break;
                    case "Lucas Eduardo":
                        openLinkedin("https://www.linkedin.com/in/lucas-eduardo-romero/");
                    default:
                }
            }
        });

        holder.email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = holder.getAdapterPosition();
                switch (teamList.get(position).getNome()) {
                    case "Gabriel Bastos":
                        dialogEmail("gabriel.bastos2332@gmail.com");
                        break;
                    case "Felipe Jung":
                        dialogEmail("felipemattoseu@gmail.com");
                        break;
                    case "Luis Eduardo":
                        dialogEmail("Le.barros20166@hotmail.com");
                        break;
                    case "Tiago Relembrando":
                        dialogEmail("thellebrandtsilva@gmail.com");
                        break;
                    case "Naíra de Moura":
                        dialogEmail("nairamsouza@gmail.com");
                        break;
                    case "Lucas Eduardo":
                        dialogEmail("lucasromero.cba@Hotmail.com");
                    default:
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return teamList.size();
    }

    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = mContext.getPackageManager();
        boolean app_installed;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }

    private void openWhatsappContact(String number) {

        PackageManager packageManager = mContext.getPackageManager();
        Intent i = new Intent(Intent.ACTION_VIEW);

        if (appInstalledOrNot("com.whatsapp")) {
            try {
                String url = "https://api.whatsapp.com/send?phone=" + number + "&text=" + URLEncoder.encode("", "UTF-8");
                i.setPackage("com.whatsapp");
                i.setData(Uri.parse(url));
                if (i.resolveActivity(packageManager) != null) {
                    mContext.startActivity(i);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            dialogWhatsApp(number);
        }

    }

    private void openFacebook(String profile) {

        PackageManager packageManager = mContext.getPackageManager();
        Intent i = new Intent(Intent.ACTION_VIEW);

        if (appInstalledOrNot("com.facebook.katana")) {
            try {
                i.setPackage("com.facebook.katana");
                i.setData(Uri.parse(profile));
                if (i.resolveActivity(packageManager) != null) {
                    mContext.startActivity(i);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            dialogFace(profile);
        }

    }

    private void openLinkedin(String profile){

        PackageManager packageManager = mContext.getPackageManager();
        Intent i = new Intent(Intent.ACTION_VIEW);

        if (appInstalledOrNot("com.linkedin.android")) {
            try {
                i.setPackage("com.linkedin.android");
                i.setData(Uri.parse(profile));
                if (i.resolveActivity(packageManager) != null) {
                    mContext.startActivity(i);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            dialogLinkedin(profile);
        }

    }

    private void dialogWhatsApp(String numero) {

        AlertDialog.Builder alerta = new AlertDialog.Builder(mContext);
        alerta.setTitle("");
        alerta.setIcon(R.mipmap.ic_check_dialog);
        alerta.setMessage(numero);
        alerta.setCancelable(true);
        AlertDialog AlertDialog = alerta.create();
        AlertDialog.show();

    }

    private void dialogLinkedin(String link) {

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
        mContext.startActivity(intent);

    }

    private void dialogEmail(String numero) {

        AlertDialog.Builder alerta = new AlertDialog.Builder(mContext);
        alerta.setTitle("");
        alerta.setIcon(R.mipmap.ic_check_dialog);
        alerta.setMessage(numero);
        alerta.setCancelable(true);
        AlertDialog AlertDialog = alerta.create();
        AlertDialog.show();

    }

    private void dialogFace(String link) {

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
        mContext.startActivity(intent);
    }

    class SobreNosViewHolder extends RecyclerView.ViewHolder {

        ImageView background, whatsapp, linkedin, email, face;
        CircleImageView foto;
        TextView nome, idade, frase;

        SobreNosViewHolder(View itemView) {
            super(itemView);

            nome = itemView.findViewById(R.id.nome);
            idade = itemView.findViewById(R.id.idade);
            frase = itemView.findViewById(R.id.frase);
            background = itemView.findViewById(R.id.background);
            foto = itemView.findViewById(R.id.foto);
            whatsapp = itemView.findViewById(R.id.whatsapp);
            linkedin = itemView.findViewById(R.id.linkedin);
            email = itemView.findViewById(R.id.email);
            face = itemView.findViewById(R.id.face);

        }
    }

}
