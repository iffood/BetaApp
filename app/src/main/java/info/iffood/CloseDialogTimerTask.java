package info.iffood;

import android.app.AlertDialog;

import java.util.TimerTask;

public class CloseDialogTimerTask extends TimerTask {
    private AlertDialog ad;

    CloseDialogTimerTask(AlertDialog ad){
        this.ad = ad;
    }
    @Override
    public void run (){
        if(ad.isShowing()){
        ad.dismiss();
        }
    }

}
