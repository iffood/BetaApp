package info.iffood;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Timer;

public class InformacoesPedidoActivity extends AppCompatActivity {

    private String parametros = "";
    private String URLPedidos = "http://iffood.info/_core/_controller/pedidos.php";
    private ArrayList<String> nomes = new ArrayList<>();
    private String local;
    private String data;
    private ArrayList<String> quantidade = new ArrayList<>();
    private ArrayList<String> produtosLista = new ArrayList<>();
    private TextView setData;
    private TextView setLocal;
    private String avaliacao;
    private String IDPedido;
    private String status;
    private boolean seguir = true;
    private String comentario;
    private String observation;
    private boolean seguirAvaliacao = false;
    private ListView lista;
    private int verifier = 0;
    private Handler h = new Handler();
    private int delay = 10000; //1 second=1000 milisecond, 15000=15seconds
    private Runnable runnable;
    private SharedPreferences mPreferences;
    private SharedPreferences.Editor mEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_informacoes_pedido);

        SharedPreferences preferences = getSharedPreferences("agoravai", 0);
        IDPedido = preferences.getString("id_pedido", "Não recebido");

        mPreferences = PreferenceManager.getDefaultSharedPreferences(InformacoesPedidoActivity.this);
        //IDPedido = mPreferences.getString("id_pedido", "Não recebido");
        status = mPreferences.getString("status", "Não recebido");

        Log.i("status1", status);
        Log.i("ID PEDIDO INFO", IDPedido);

        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        lista = findViewById(R.id.lista);

        setLocal = findViewById(R.id.setLocal);
        setData = findViewById(R.id.setData);
        Button avaliar = findViewById(R.id.avaliar);
        Button cancelar = findViewById(R.id.cancelar);

        avaliar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogAvaliacao();
            }
        });

        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Cancela pedido

                if (!status.equals("cancelado") && !status.equals("entregado")) {

                    new android.support.v7.app.AlertDialog.Builder(InformacoesPedidoActivity.this)
                            .setTitle("Cancelar pedido")
                            .setMessage("Tem certeza que deseja cancelar o pedido?")
                            .setPositiveButton("Cancelar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    //CANCELAR

                                    ConnectivityManager connectivityManager = (ConnectivityManager)
                                            getSystemService(Context.CONNECTIVITY_SERVICE);
                                    assert connectivityManager != null;
                                    NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

                                    if (networkInfo != null && networkInfo.isConnected()) {

                                        try {
                                            cancelarPedido();
                                            Intent intent = new Intent(InformacoesPedidoActivity.this, MainActivity.class);
                                            finish();
                                            mPreferences = PreferenceManager.getDefaultSharedPreferences(InformacoesPedidoActivity.this);
                                            mEditor = mPreferences.edit();
                                            mEditor.putString("status", "cancelado").apply();
                                            startActivity(intent);

                                            try {
                                                Thread.sleep(500);
                                            } catch (InterruptedException e) {
                                                e.printStackTrace();
                                            }

                                            new InformacoesPedidoActivity.solicitaDados().execute(URLPedidos);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    } else {
                                        Snackbar snackbar = Snackbar.make(findViewById(R.id.relativeInformacoes), "Nenhuma conexão detectada! Habilite a internet e tente novamente.", Snackbar.LENGTH_LONG);
                                        snackbar.show();
                                    }
                                }
                            })
                            .setNegativeButton("Não", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    //Toast.makeText(InformacoesPedidoActivity.this, "Não", Toast.LENGTH_SHORT).show();
                                }
                            })
                            .show();
                } else {
                    Snackbar snackbar = Snackbar.make(findViewById(R.id.relativeInformacoes), "Pedido já finalizado!", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }

            }
        });


    }

    private void getPedido() {

        parametros = "action=" + "getPedido" + "&pedido=" + IDPedido;

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_pedidos, menu);
        return true;
    }


    @Override
    protected void onStart() {

        SharedPreferences preferences = getSharedPreferences("agoravai", 0);
        if (preferences.contains("id_pedido")) {
            IDPedido = preferences.getString("id_pedido", "Não recebido");
        }

        mPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        //IDPedido = mPreferences.getString("id_pedido", "Não recebido");
        status = mPreferences.getString("status", "Não recebido");

        Log.i("status1", status);
        Log.i("ID PEDIDO INFO", IDPedido);

        ConnectivityManager connectivityManager = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {

            try {

                getInfoPedido();

                new solicitaDados().execute(URLPedidos);

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            //Toast.makeText(InformacoesPedidoActivity.this, "Nenhuma conexão com internet foi detectada! Habilite a internet!", Toast.LENGTH_SHORT).show();
            Snackbar snackbar = Snackbar.make(findViewById(R.id.relativeInformacoes), "Nenhuma conexão com internet foi detectada! Habilite a internet!", Snackbar.LENGTH_LONG);
            snackbar.show();
        }


        super.onStart();
    }

    private void getInfoPedido() {

        parametros = "action=getInfoPedido&id=" + IDPedido;

    }

    @Override
    protected void onPause() {
        h.postDelayed(new Runnable() {
            @Override
            public void run() {

                SharedPreferences preferences = getSharedPreferences("agoravai", 0);
                if (preferences.contains("id_pedido")) {
                    IDPedido = preferences.getString("id_pedido", "Não recebido");
                }

                //mPreferences = PreferenceManager.getDefaultSharedPreferences(InformacoesPedidoActivity.this);

                //IDPedido = mPreferences.getString("id_pedido", "Não recebido");

                getInfoPedido();

                new InformacoesPedidoActivity.solicitaDados().execute(URLPedidos);

                runnable = this;
                h.postDelayed(runnable, delay);
            }
        }, delay);
        super.onPause();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.refresh:
                Intent intentPedidos = new Intent(getApplicationContext(), InformacoesPedidoActivity.class);
                finish();
                startActivity(intentPedidos);
            default:
        }
        return true;
    }

    private void dialogAvaliacao() {

        //if(seguirAvaliacao) {

        final AlertDialog.Builder mBuilder = new AlertDialog.Builder(InformacoesPedidoActivity.this);
        @SuppressLint("InflateParams") final View subView = getLayoutInflater().inflate(R.layout.avaliacao_dialog, null);
        mBuilder.setTitle("Por favor, avalie a entrega!");

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Por favor, avalie a entrega!");
        builder.setView(subView);
        final AlertDialog alertDialog = builder.create();

        final EditText infoComentario = subView.findViewById(R.id.comentarioAvalicao);

        infoComentario.setImeOptions(EditorInfo.IME_ACTION_DONE);
        infoComentario.setRawInputType(InputType.TYPE_CLASS_TEXT);

        RatingBar ratingBar = subView.findViewById(R.id.ratingBar);
        ratingBar.setStepSize(1);
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                avaliacao = String.valueOf((int) rating);
            }
        });

        builder.setPositiveButton("Enviar avaliação", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                try {
                    comentario = infoComentario.getText().toString();
                    Log.i("AVALIACAO E COMENTARIO", avaliacao + " " + comentario);
                    setAvaliacao(avaliacao, comentario);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });
        builder.show();
    }
    //}

    private void cancelarPedido() {

        URLPedidos = "http://iffood.info/_core/_controller/pedidos.php";

        parametros = "action=" + "cancelarPedido" + "&id=" + IDPedido;

    }

    private void setAvaliacao(String avaliacao, String comentario) {

        ConnectivityManager connectivityManager = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {

            try {

                URLPedidos = "http://iffood.info/_core/_controller/pedidos.php";

                parametros = "action=" + "setAvaliacao" + "&id=" + IDPedido + "&avaliacao=" + avaliacao + "&msg=" + comentario;

                Log.i("PARAMETROS AVALIAÇÃO", parametros);

                seguirAvaliacao = true;

                new InformacoesPedidoActivity.solicitaDados().execute(URLPedidos);

            } catch (Exception e) {

                e.printStackTrace();

            }

        } else {
            Snackbar snackbar = Snackbar.make(findViewById(R.id.relativeInformacoes), "Nenhuma conexão detectada! Habilite a internet e tente novamente.", Snackbar.LENGTH_LONG);
            snackbar.show();
        }


    }

    private void dialogAceitado() {

        AlertDialog.Builder alerta = new AlertDialog.Builder(InformacoesPedidoActivity.this);
        alerta.setTitle("Pedido Aceito");
        alerta.setIcon(R.drawable.ic_check_alertdialog_web);
        alerta.setMessage("Um vendedor aceitou seu pedido e está a caminho! Aguarde-o no local de entrega escolhido :)");
        alerta.setCancelable(false);
        alerta.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog AlertDialog = alerta.create();
        AlertDialog.show();

        seguir = false;
    }

    private void dialogCancelado() {

        AlertDialog.Builder alerta = new AlertDialog.Builder(InformacoesPedidoActivity.this);
        alerta.setTitle("Pedido cancelado");
        alerta.setIcon(R.drawable.ic_check_alertdialog_web);
        alerta.setMessage("Você cancelou este pedido.");
        alerta.setCancelable(false);
        alerta.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog AlertDialog = alerta.create();
        AlertDialog.show();

        seguir = false;

    }

    private void dialogRecusado(String observation) {

        AlertDialog.Builder alerta = new AlertDialog.Builder(InformacoesPedidoActivity.this);
        alerta.setTitle("Pedido Rejeitado :(");
        alerta.setIcon(R.drawable.ic_check_alertdialog_web);
        alerta.setMessage("Infelizmente o vendedor teve que recusar seu pedido. Tente fazer outro. \nMotivo da recusa: " + observation);
        alerta.setCancelable(false);
        alerta.setPositiveButton("Ok :(", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog AlertDialog = alerta.create();
        AlertDialog.show();

        seguir = false;

    }

    private void dialogEntregado() {

        AlertDialog.Builder alerta = new AlertDialog.Builder(InformacoesPedidoActivity.this);
        alerta.setTitle("Pedido Entregue!");
        alerta.setIcon(R.drawable.ic_check_alertdialog_web);
        alerta.setMessage("Seu pedido foi entregue com sucesso!");
        alerta.setCancelable(false);
        alerta.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog AlertDialog = alerta.create();
        AlertDialog.show();

        seguir = false;

    }

    private void dialogAguardando() {

        AlertDialog.Builder alerta = new AlertDialog.Builder(InformacoesPedidoActivity.this);
        alerta.setTitle("Pedido em aguardo");
        alerta.setIcon(R.drawable.ic_check_alertdialog_web);
        alerta.setMessage("A qualquer momento um vendedor pode aceitar seu pedido. :)");
        alerta.setCancelable(false);
        alerta.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog AlertDialog = alerta.create();

        if (!((Activity) InformacoesPedidoActivity.this).isFinishing()) {
            AlertDialog.show();
        }


        seguir = false;

    }


    @SuppressLint("StaticFieldLeak")
    private class solicitaDados extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if (seguirAvaliacao) {
                final ProgressDialog progressDialog = new ProgressDialog(InformacoesPedidoActivity.this);
                progressDialog.setTitle("Envio de avaliação");
                progressDialog.setMessage("Enviando sua avaliação, por favor tenha paciência :)");
                progressDialog.setCancelable(false);
                progressDialog.show();
                Timer t = new Timer();
                t.schedule(new CloseDialogTimerTask(progressDialog), 4100);
            }
        }

        @Override
        protected String doInBackground(String... urls) {
            return Connection.postDados(urls[0], parametros);
        }

        @Override
        protected void onPostExecute(final String resposta) {

            if (resposta != null && resposta.contains("status")) {

                Log.i("resposta", resposta);

                quantidade.clear();
                nomes.clear();
                produtosLista.clear();
                try {
                    try {
                        JSONObject json = new JSONObject(resposta);

                        data = json.optString("data");
                        local = json.optString("local");
                        status = json.optString("status");
                        observation = json.optString("observacao");

                        mPreferences = PreferenceManager.getDefaultSharedPreferences(InformacoesPedidoActivity.this);
                        mEditor = mPreferences.edit();
                        mEditor.putString("status", status);
                        mEditor.putString("observation", observation);
                        mEditor.apply();

                        JSONArray jsonArray = json.getJSONArray("produtos");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject pedido = jsonArray.getJSONObject(i);
                            quantidade.add(pedido.get("quantidade").toString());
                            JSONObject produto = pedido.getJSONObject("produto");
                            nomes.add(produto.get("nome").toString());

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    Log.i("Llocal", local);
                    Log.i("Lquantidade", quantidade.toString());
                    Log.i("data", data);
                    Log.i("Lnomes", nomes.toString());

                    for (int count = 0; count < nomes.size(); count++) {
                        produtosLista.add(nomes.get(count) + ": " + quantidade.get(count));
                    }

                    setData.setText(data);
                    setLocal.setText(local);


                    Log.i("Lista de produtos", produtosLista.toString());

                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getApplicationContext(), R.layout.textview, produtosLista);
                    lista.setAdapter(arrayAdapter);
                    arrayAdapter.notifyDataSetChanged();

                } catch (Exception e) {
                    e.printStackTrace();
                }


                if (seguir) {
                    if (status.equals("aguardando")) {
                        dialogAguardando();
                    }
                    if (status.equals("entregado")) {
                        dialogEntregado();
                    }
                    if (status.equals("recusado")) {
                        dialogRecusado(observation);
                    }
                    if (status.equals("aceitado")) {
                        dialogAceitado();
                    }
                    if (status.equals("cancelado")) {
                        dialogCancelado();
                    }
                }

                final Runnable runnable = new Runnable() {

                    @Override
                    public void run() {

                        if (seguirAvaliacao) {
                            AlertDialog.Builder alerta = new AlertDialog.Builder(InformacoesPedidoActivity.this);
                            alerta.setTitle("Envio de avaliação");
                            alerta.setIcon(R.drawable.ic_check_alertdialog_web);
                            alerta.setMessage("Sua avaliação foi enviada com sucesso!");
                            alerta.setCancelable(false);
                            alerta.setPositiveButton("Beleza", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            AlertDialog AlertDialog = alerta.create();
                            AlertDialog.show();
                            seguirAvaliacao = false;
                        }

                    }
                };

                final Handler h = new Handler();
                h.removeCallbacks(runnable); // cancel the running action (the
                // hiding process)
                h.postDelayed(runnable, 4100);

            } else {
                Snackbar snackbar = Snackbar.make(findViewById(R.id.relativeInformacoes), "Erro ao carregar dados :( Tente novamente.", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

    }
}
