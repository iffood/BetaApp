package info.iffood;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ComprarActivity extends AppCompatActivity {

    private static final String URLPedidos = "http://iffood.info/_core/_controller/pedidos.php";
    private String parametros = "";
    private ArrayList<String> IDSProdutos = new ArrayList<>();
    private ArrayList<String> Quantidades = new ArrayList<>();
    private Spinner spinnerTroco;
    private String lugarEscolhido;
    private Editable complemento;
    private String nomeUsuario;
    private boolean prosseguirPedido = true;
    private boolean antesPedir = true;
    private SharedPreferences mPreferences;
    private SharedPreferences.Editor mEditor;
    private List<String> locais = new ArrayList<>();
    private static final String URLLocais = "http://iffood.info/_core/_controller/locais.php";
    private ArrayList<String> idLocal = new ArrayList<>();
    private String pagamento;
    private String[] notas = {"Notas", "R$2,00", "R$5,00", "R$10,00", "R$20,00", "R$50,00", "R$100,00"};
    private String troco;
    private ProgressDialog progressDialog;
    private Spinner mSpinner;

    @SuppressLint("CommitPrefEdits")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comprar);

        ConnectivityManager connectivityManager = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {

            getLocais();

            new solicitaDados().execute(URLLocais);

        } else {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Nenhuma conexão com internet foi detectada! Habilite a internet e tente novamente!", Snackbar.LENGTH_LONG);
            snackbar.show();
        }


        mSpinner = findViewById(R.id.spinner);

        //----------------------------------------------------------------------------------------------------------------------
        //RECEBER DADOS

        mPreferences = PreferenceManager.getDefaultSharedPreferences(ComprarActivity.this);
        mEditor = mPreferences.edit();

        nomeUsuario = mPreferences.getString("nome_usuario", String.valueOf("nome"));
        //float vtot = mPreferences.getFloat("valor_total", 0);
        int ntrufas = mPreferences.getInt("numero_trufas", 0);
        Log.i("Numero Trufas", "Resu" + ntrufas);

        //----------------------------------------------------------------------------------------------------------------------
        //TOOLBAR
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        //toolbar.setLogo(R.drawable.iffood_launcher);
        //----------------------------------------------------------------------------------------------------------------------
        //BOTTOM NAVIGATION
        /*BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.cardapio:
                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        finish();
                        break;
                    case R.id.cart:
                        startActivity(new Intent(getApplicationContext(), ComprarActivity.class));
                        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                        finish();
                    default:
                }
                return false;
            }
        });*/

        spinnerTroco = findViewById(R.id.spinnerTroco);
        ArrayAdapter<String> adapterTroco = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item,
                notas);
        adapterTroco.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTroco.setAdapter(adapterTroco);

        //Log.i("Locais", locais.toString());

        final EditText complementoEdit = findViewById(R.id.sala);

        complementoEdit.setImeOptions(EditorInfo.IME_ACTION_DONE);
        complementoEdit.setRawInputType(InputType.TYPE_CLASS_TEXT);

        Button finalizarCompra = findViewById(R.id.finalizarCompra);
        finalizarCompra.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {

                //FORMA DE PAGAMENTO
                RadioGroup radioGroup = findViewById(R.id.radioGroup);

                int idFormaPagamentoEscolhido = radioGroup.getCheckedRadioButtonId();

                final RadioButton radioButtonEscolhido = findViewById(idFormaPagamentoEscolhido);

                if (radioButtonEscolhido != null) {
                    pagamento = radioButtonEscolhido.getText().toString();
                    Log.i("TESTES ", radioButtonEscolhido.getText().toString());
                }

                try {

                    Integer indexLocal = mSpinner.getSelectedItemPosition();
                    lugarEscolhido = locais.get(indexLocal);

                    if (lugarEscolhido == null) {
                        Snackbar snackbar = Snackbar.make(v, "Erro ao definir local de entrega. Por favor, tente novamente!", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }

                    Integer indexTroco = spinnerTroco.getSelectedItemPosition();
                    troco = notas[indexTroco];

                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (pagamento != null) {
                        if (pagamento.equals("Dinheiro")) {
                            switch (troco) {
                                case "R$2,00":
                                    troco = "2";
                                    break;
                                case "R$5,00":
                                    troco = "5";
                                    break;
                                case "R$10,00":
                                    troco = "10";
                                    break;
                                case "R$20,00":
                                    troco = "20";
                                    break;
                                case "R$50,00":
                                    troco = "50";
                                    break;
                                case "R$100,00":
                                    troco = "100";
                                default:
                            }
                        } else {
                            troco = null;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                try {
                    complemento = complementoEdit.getText();
                    Log.i("AA lugarEscolhido", lugarEscolhido);
                    Log.i("AA complemento", complemento.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //----------------------------------------------------------------------------------------------------------------------
                if (radioButtonEscolhido != null) {

                    for (int i = 0; i < locais.size(); i++) {
                        if (lugarEscolhido.equals(locais.get(i))) {
                            lugarEscolhido = idLocal.get(i);
                        }
                    }

                    Log.i("PAYMENT", radioButtonEscolhido.getText().toString());

                    abrirDialog(prosseguirPedido);

                } else {
                    Snackbar snackbar = Snackbar.make(v, "Por favor, preencha os campos necessários!", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
                //----------------------------------------------------------------------------------------------------------------------


            }
        });

    }

    @Override
    protected void onStart() {

        //---------------------------------------------------------------------------------------------------------------------
        //ORGANIZAR DADOS PARA UTILIZA-LOS COMO PARAMETROS
        new Thread(new Runnable() {
            @Override
            public void run() {
                mPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                String words = mPreferences.getString("id", "Não recebido");
                String[] itemsWords = words.split(",");
                Collections.addAll(IDSProdutos, itemsWords);

                String valores = mPreferences.getString("valores", "Não recebido");
                String[] items = valores.split(",");
                Collections.addAll(Quantidades, items);

                Log.i("AEE", Quantidades.toString());

                Log.i("AEEE", IDSProdutos.toString());
            }
        }).start();


        super.onStart();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_pedidos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.refresh:
                Intent intentPedidos = new Intent(getApplicationContext(), ComprarActivity.class);
                finish();
                startActivity(intentPedidos);
            default:
        }
        return true;
    }

    private void abrirDialog(boolean prosseguir) {

        if (prosseguir) {
            new android.support.v7.app.AlertDialog.Builder(this)
                    .setTitle("Confirmação de compra")
                    .setMessage("Tem certeza que deseja finalizar a compra?")
                    .setPositiveButton("Absoluta!", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            ConnectivityManager connectivityManager = (ConnectivityManager)
                                    getSystemService(Context.CONNECTIVITY_SERVICE);
                            assert connectivityManager != null;
                            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

                            if (networkInfo != null && networkInfo.isConnected()) {

                                fazerPedido(nomeUsuario, lugarEscolhido, complemento.toString(), pagamento, troco, IDSProdutos, Quantidades);
                                prosseguirPedido = false;
                                antesPedir = false;
                                new solicitaDados().execute(URLPedidos);
                            } else {
                                Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Nenhuma conexão com internet foi detectada! Habilite a internet e tente novamente!", Snackbar.LENGTH_LONG);
                                snackbar.show();
                            }
                        }
                    })
                    .setNegativeButton("Quero mais trufas", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            antesPedir = true;
                        }
                    })
                    .show();

        }
    }

    private void fazerPedido(String cliente, String local, String complemento, String payment, String troco, ArrayList<String> produtos, ArrayList<String> quantidades) {

        parametros = "action=" + "pedir" + "&cliente=" + cliente + "&local=" + local + "&complemento=" + complemento + "&payment=" + payment + "&troco=" + troco;

        int x = 1;
        for (int i = 0; i < quantidades.size(); i++) {
            if (!quantidades.get(i).equals("0")) {
                parametros += "&produto" + String.valueOf(x) + "=" + produtos.get(i);
                parametros += "&quantidade" + String.valueOf(x) + "=" + quantidades.get(i);
                x++;
            }
        }
        Log.i("TESTE PARAMETROS", parametros);
    }

    private void getLocais() {

        parametros = "action=" + "getLocais";

    }

    @SuppressLint("StaticFieldLeak")
    private class solicitaDados extends AsyncTask<String, Void, String> {
        @SuppressLint("CommitPrefEdits")
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if (!antesPedir && !locais.isEmpty()) {

                progressDialog = new ProgressDialog(ComprarActivity.this);
                progressDialog.setTitle("Envio de Dados");
                progressDialog.setMessage("Enviando seu pedido, por favor tenha paciência :)");
                progressDialog.setCancelable(false);
                progressDialog.show();

                if (getCurrentFocus() != null) {
                    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    assert inputMethodManager != null;
                    inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                }

            }
        }

        @Override
        protected String doInBackground(String... urls) {
            return Connection.postDados(urls[0], parametros);
        }

        @Override
        protected void onPostExecute(String resposta) {

            if (resposta != null && antesPedir) {

                try {
                    JSONArray pedidos = new JSONArray(resposta);
                    for (int i = 0; i < pedidos.length(); i++) {
                        JSONObject pedido = pedidos.getJSONObject(i);
                        locais.add(pedido.get("nome").toString());
                        idLocal.add(pedido.get("id").toString());
                    }

                    Log.i("LOCAIS", locais.toString() + " | " + idLocal.toString());

                    String[] locaisDefinitive = new String[locais.size()];

                    for (int i = 0; i < locais.size(); i++) {
                        locaisDefinitive[i] = locais.get(i);
                    }

                    Log.i("LocaisDefinitive", Arrays.toString(locaisDefinitive));

                    ArrayAdapter adapter = new ArrayAdapter<>(ComprarActivity.this, android.R.layout.simple_spinner_item, locaisDefinitive);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    mSpinner.setAdapter(adapter);
                    adapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else{
                getLocais();

                new solicitaDados().execute(URLLocais);
            }

            if (!antesPedir && !locais.isEmpty()) {

                Log.i("ANTESPEDIR", String.valueOf(antesPedir));
                final Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.dismiss();
                        AlertDialog.Builder alerta = new AlertDialog.Builder(ComprarActivity.this);
                        alerta.setTitle("Envio de pedido");
                        alerta.setIcon(R.drawable.ic_check_alertdialog_web);
                        alerta.setMessage("Seu pedido foi enviado com sucesso!");
                        alerta.setCancelable(false);
                        alerta.setPositiveButton("Beleza", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(ComprarActivity.this, InformacoesPedidoActivity.class);
                                mPreferences = PreferenceManager.getDefaultSharedPreferences(ComprarActivity.this);
                                mEditor = mPreferences.edit();
                                mEditor.remove("status").apply();
                                finish();
                                startActivity(intent);
                            }
                        });
                        AlertDialog AlertDialog = alerta.create();
                        AlertDialog.show();
                    }
                };
                final Handler h = new Handler();
                h.removeCallbacks(runnable);
                h.postDelayed(runnable, 2100);
            }

            if (!antesPedir) {
                Log.i("ID PEDIDOO: ", resposta);

                SharedPreferences preferences = getSharedPreferences("agoravai", 0);
                @SuppressLint("CommitPrefEdits") SharedPreferences.Editor editor = preferences.edit();
                editor.putString("id_pedido" ,resposta);
                editor.apply();

                antesPedir = true;

            }

        }

    }

}
