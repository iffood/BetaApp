package info.iffood;

public class IffoodTeam {

    private String nome;
    private String idade;
    private String frase;
    private int foto, background;

    IffoodTeam(String nome, String idade, String frase, int foto, int background) {
        this.nome = nome;
        this.idade = idade;
        this.frase = frase;
        this.foto = foto;
        this.background = background;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getIdade() {
        return idade;
    }

    public void setIdade(String idade) {
        this.idade = idade;
    }

    public String getFrase() {
        return frase;
    }

    public void setFrase(String frase) {
        this.frase = frase;
    }

    public int getFoto() {
        return foto;
    }

    public void setFoto(int foto) {
        this.foto = foto;
    }

    public int getBackground() {
        return background;
    }

    public void setBackground(int background) {
        this.background = background;
    }
}
