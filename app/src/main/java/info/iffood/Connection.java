package info.iffood;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class Connection {

    public static String postDados(String UrlPost, String parametros){
        URL url;
        HttpURLConnection connection = null;

        try{

            url = new URL(UrlPost);
            connection = (HttpURLConnection) url.openConnection();

            connection.setRequestMethod("POST");

            connection.setRequestProperty("Content-type", "application/x-www-form-urlencoded");

            connection.setRequestProperty("Content-Lenght", "" + Integer.toString(parametros.getBytes().length));

            connection.setRequestProperty("Content-Language", "pt-BR");

            connection.setUseCaches(false);

            connection.setDoInput(true);

            connection.setDoOutput(true);

            connection.setChunkedStreamingMode(0);


            /*DataOutputStream dataOutputStream = new DataOutputStream(connection.getOutputStream());
            dataOutputStream.writeBytes(parametros);
            dataOutputStream.flush();
            dataOutputStream.close();*/

            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
            outputStreamWriter.write(parametros);
            outputStreamWriter.flush();

            InputStream inputStream = connection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            String linha;
            StringBuilder resposta = new StringBuilder();

            while((linha = bufferedReader.readLine()) != null){
                resposta.append(linha);
            }

            bufferedReader.close();

            return resposta.toString();

        }catch (Exception e){

            return null;

        }finally {

            if(connection != null){
                connection.disconnect();
            }

        }


    }
}
