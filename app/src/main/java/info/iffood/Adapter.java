package info.iffood;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

public class Adapter extends RecyclerView.Adapter<viewHolder> {
    private static final String TAG = "Adapter";
    private ArrayList<String> quantidadeProdutos = new ArrayList<>();
    private ArrayList<String> id = new ArrayList<>();
    private ArrayList<Integer> valores = new ArrayList<>();
    private ArrayList<String> price = new ArrayList<>();
    private List<Source> objectList;
    private static final String URLProdutos = "http://iffood.info/_core/_controller/produtos.php";
    private static final String parametros = "action=getProdutos";
    private float valor_trufas_total = 0;
    private int numero_trufas_total = 0;
    private Context mContext;
    private SharedPreferences mPreferences;
    private SharedPreferences.Editor mEditor;
    private boolean prosseguir;
    private ArrayList<Float> preco = new ArrayList<>();
    private View view;

    Adapter(List<Source> objectList, Context mContext, View view) {
        this.mContext = mContext;
        this.objectList = objectList;
        this.view = view;
    }

    @Override
    public viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item, parent, false);
        return new viewHolder(view);
    }

    @Override
    public void onBindViewHolder(final viewHolder holder, int position) {

        ConnectivityManager connectivityManager = (ConnectivityManager)
                mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {

            new Adapter.pegadoraDados().execute(URLProdutos);
        } else {
            Snackbar snackbar = Snackbar.make(view, "Nenhuma conexão com a internet detectada!", Snackbar.LENGTH_LONG);
            snackbar.show();
        }

        holder.title.setText(objectList.get(position).getTitle());
        holder.price.setText(objectList.get(position).getPrice());
        if(!objectList.get(position).getImagem().isEmpty()) {
            Picasso.with(mContext).load(objectList.get(position).getImagem()).fit().centerCrop().into(holder.imagem, new Callback() {
                @Override
                public void onSuccess() {
                    holder.loadingImage.setVisibility(View.GONE);
                }

                @Override
                public void onError() {
                    holder.loadingImage.setVisibility(View.VISIBLE);
                }
            });
        }

        holder.buttonmais.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                valor_trufas_total = 0;

                int position = holder.getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    Log.i("TESTE +", "POSIÇÃO: " + position);
                    for (int i = 0; i < id.size(); i++) {
                        if (position == i) {

                            connect();
                            prosseguir = false;
                            try {

                                if (valores.get(i) == 50) {
                                    holder.numeroTrufas.setText("50");
                                } else {
                                    if (!verificaQuantidades(quantidadeProdutos, position, valores.get(i))) {
                                        valores.set(i, (valores.get(i) + 1));
                                        numero_trufas_total++;
                                        holder.numeroTrufas.setText(String.valueOf(valores.get(i)));
                                    }

                                    for (int k = 0; k < preco.size(); k++) {
                                        if (valores.get(k) != null && valores.get(k) != 0) {
                                            valor_trufas_total += valores.get(k) * preco.get(k);
                                        }
                                    }
                                    salvarDados(numero_trufas_total, valor_trufas_total);

                                }

                                Log.i("TOTAL", String.valueOf(valor_trufas_total));


                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    }
                    //Log.i("array", quantidadeProdutos.toString());
                    //Log.i("id", id.toString());
                    //Log.i("PRECO valores", valores.toString());
                }
            }

        });

        holder.buttonmenos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                valor_trufas_total = 0;

                int position = holder.getAdapterPosition();
                if(position != RecyclerView.NO_POSITION) {
                    Log.i("TESTE -", "POSIÇÃO: " + position);
                    for (int i = 0; i < id.size(); i++) {
                        if (position == i) {

                            connect();
                            prosseguir = true;
                            try {

                                if (valores.get(i) == 0) {
                                    holder.numeroTrufas.setText("0");
                                } else {

                                    valores.set(i, (valores.get(i) - 1));
                                    numero_trufas_total--;
                                    holder.numeroTrufas.setText(String.valueOf(valores.get(i)));

                                    for (int k = 0; k < preco.size(); k++) {
                                        if (valores.get(k) != null && valores.get(k) != 0) {
                                            valor_trufas_total -= valores.get(k) * preco.get(k) * -1;
                                        }
                                    }
                                    salvarDados(numero_trufas_total, valor_trufas_total);
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    }
                    //Log.i("array", quantidadeProdutos.toString());
                    //Log.i("id", id.toString());
                    //Log.i("valores", valores.toString());
                }
            }
        });
        //Log.i("Resu", "Numero trufas total: " + numero_trufas_total);
    }

    @Override
    public int getItemCount() {
        return objectList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    private void salvarDados(final int numero_trufas_total, final float valor_trufas_total) {

        new Thread(new Runnable() {
            @Override
            public void run() {

        //Log.i("ARRAYS", id.toString());
        //Log.i("ARRAYS", valores.toString());

        mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        mEditor = mPreferences.edit();
        mEditor.remove("id").apply();
        mEditor.remove("valores").apply();

        try {
            mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
            mEditor = mPreferences.edit();

            mEditor.putInt("numero_trufas", numero_trufas_total);
            mEditor.putFloat("valor_total", valor_trufas_total);

            StringBuilder stringBuilderIds = new StringBuilder();
            for (String s : id) {
                stringBuilderIds.append(s);
                stringBuilderIds.append(",");
            }

            StringBuilder stringBuilderValores = new StringBuilder();
            for (Integer s : valores) {
                stringBuilderValores.append(s);
                stringBuilderValores.append(",");
            }

            //Log.i("Strings", stringBuilderIds.toString());
            //Log.i("Strings", stringBuilderValores.toString());
            mEditor.putString("id", stringBuilderIds.toString());
            mEditor.putString("valores", stringBuilderValores.toString());

            mEditor.apply();
        }catch (Exception e){
            e.printStackTrace();
        }

        //Log.d(TAG, "salvarDados: NUMERO TRUFAS: " + numero_trufas_total);
        //Log.d(TAG, "salvarDados: VALOR TOTAL: " + valor_trufas_total);

            }
        }).start();
    }


    private void connect() {
        ConnectivityManager connectivityManager = (ConnectivityManager)
                mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {

            new Adapter.pegadoraDados().execute(URLProdutos);
        } else {
            //Toast.makeText(mContext, "Nenhuma conexão com internet detectada", Toast.LENGTH_SHORT).show();
            Snackbar snackbar = Snackbar.make(view, "Nenhuma conexão com internet detectada!", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    private boolean verificaQuantidades(ArrayList<String> quantidade, int IDTrufa, int numero_trufas) {

        for (int i = 0; i < quantidade.size(); i++) {
            if ((IDTrufa) == (i)) {
                if (numero_trufas == Integer.parseInt(quantidade.get(i))) {
                    Snackbar snackbar = Snackbar.make(view, "Número de produtos disponíveis no momento alcançado!", Snackbar.LENGTH_LONG);
                    snackbar.show();
                    prosseguir = true;
                }
            }
        }
        return prosseguir;
    }

    @SuppressLint("StaticFieldLeak")
    private class pegadoraDados extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {
            return Connection.postDados(urls[0], parametros);
        }

        @Override
        protected void onPostExecute(String resultado) {

            if (resultado != null) {

                id.clear();
                quantidadeProdutos.clear();
                price.clear();
                preco.clear();

                try {
                    JSONArray pedidos = new JSONArray(resultado);
                    for (int i = 0; i < pedidos.length(); i++) {
                        JSONObject pedido = pedidos.getJSONObject(i);
                        id.add(pedido.get("id").toString());
                        price.add(pedido.get("preco").toString());
                        if (valores.size() < id.size()) {
                            valores.add(0);
                        }
                        quantidadeProdutos.add(pedido.get("disponiveis").toString());
                    }
                    Log.i("PRECO", price.toString());

                    for(int k=0; k<price.size(); k++) {
                        float currentValor = Float.valueOf(price.get(k));
                        preco.add(currentValor);
                    }

                    //Log.i("PRECO FLOAT", preco.toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //Log.i("Teste id", id.toString());

            }

        }
    }


}



