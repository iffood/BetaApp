package info.iffood;

public class Source {

    private String title;
    private String price;
    private String imagem;

    Source(String title, String price, String imagem) {
        this.title = title;
        this.price = price;
        this.imagem = imagem;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImagem() {
        return imagem;
    }

    public void setImagem(String imagem) {
        this.imagem = imagem;
    }
}
