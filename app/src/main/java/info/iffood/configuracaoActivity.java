package info.iffood;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Timer;

public class configuracaoActivity extends AppCompatActivity {
    private EditText alterarNome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracao);

        alterarNome = findViewById(R.id.AlterarNome);
        Button botsalvarNome = findViewById(R.id.botSalvarnome);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        final SharedPreferences.Editor editor = sharedPreferences.edit();
        botsalvarNome.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String nome = alterarNome.getText().toString();
                if (nome.isEmpty()) {
                    Toast.makeText(configuracaoActivity.this, "Digite seu nome na caixa de texto", Toast.LENGTH_SHORT).show();
                } else {
                    if (nome.trim().length() != 0) {
                        editor.putString("nome_usuario", nome);
                        editor.apply();
                        final ProgressDialog progressDialog = new ProgressDialog(configuracaoActivity.this);
                        progressDialog.setTitle("Atualização de dados");
                        progressDialog.setMessage("Atualizando seus dados, por favor tenha paciência.");
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                        Timer t = new Timer();
                        t.schedule(new CloseDialogTimerTask(progressDialog), 4000);
                        alterarNome.setText("");
                        if (getCurrentFocus() != null) {
                            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                            assert inputMethodManager != null;
                            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                        }
                        final Runnable runnable = new Runnable() {

                            @Override
                            public void run() {
                                AlertDialog.Builder alerta = new AlertDialog.Builder(configuracaoActivity.this);
                                alerta.setTitle("Atualização de dados");
                                alerta.setIcon(R.drawable.ic_check_alertdialog_web);
                                alerta.setMessage("Atualização de dados concluída com sucesso!");
                                alerta.setCancelable(false);
                                alerta.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                AlertDialog AlertDialog = alerta.create();
                                AlertDialog.show();
                            }
                        };
                        final Handler h = new Handler();
                        h.removeCallbacks(runnable); // cancel the running action (the
                        // hiding process)
                        h.postDelayed(runnable, 4100);
                    } else {
                        Toast.makeText(configuracaoActivity.this, "Por favor, não coloque apenas espaço no seu nome.", Toast.LENGTH_SHORT).show();
                    }
                }


            }
        });

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }


    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
