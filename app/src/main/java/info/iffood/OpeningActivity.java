package info.iffood;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class OpeningActivity extends AppCompatActivity {

    private SharedPreferences mPreferences;
    private SharedPreferences.Editor mEditor;
    private boolean passar = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opening);

        mPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        mEditor = mPreferences.edit();

        passar = mPreferences.getBoolean("passar", false);

        if(passar){
            Intent intent = new Intent(OpeningActivity.this, MainActivity.class);
            finish();
            startActivity(intent);
        }

        Button entrar = findViewById(R.id.entrar);

        entrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(OpeningActivity.this, LoginActivity.class);
                startActivity(intent);

                mPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                mEditor = mPreferences.edit();

                passar = true;

                mEditor.putBoolean("passar", true);
                mEditor.apply();

            }
        });
    }
}
