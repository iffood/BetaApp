package info.iffood;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity_SobreNos extends AppCompatActivity {

    ArrayList<IffoodTeam> lista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main__sobre_nos);

        TextView appVersion = findViewById(R.id.textView10);
        appVersion.setText(getVersionName());

        TextView site = findViewById(R.id.site);
        site.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://iffood.info/"));
                startActivity(intent);
            }
        });

        lista = new ArrayList<>();

        lista.add(new IffoodTeam("Gabriel Bastos", "19 anos", "\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sodales risus vitae lacus imperdiet hendrerit.\"", R.drawable.bastos, R.drawable.test));
        lista.add(new IffoodTeam("Felipe Jung", "18 anos", "\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sodales risus vitae lacus imperdiet hendrerit.\"", R.drawable.felipe, R.drawable.test));
        lista.add(new IffoodTeam("Luis Eduardo", "17 anos", "\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sodales risus vitae lacus imperdiet hendrerit.\"", R.drawable.tico, R.drawable.test));
        lista.add(new IffoodTeam("Tiago Hellebrandt", "17 anos", " \"A melhor maneira de prever o futuro é criá-lo\"" + " - Peter Drucker", R.drawable.tiago, R.drawable.test));
        lista.add(new IffoodTeam("Naíra de Moura", "17 anos", "\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sodales risus vitae lacus imperdiet hendrerit.\"", R.drawable.naira, R.drawable.test));
        lista.add(new IffoodTeam("Lucas Eduardo", "16 anos", "\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sodales risus vitae lacus imperdiet hendrerit.\"", R.drawable.lucas, R.drawable.test));

        final RecyclerView recyclerView = findViewById(R.id.sobrenosRecycler);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(MainActivity_SobreNos.this, LinearLayoutManager.HORIZONTAL, false);
        final SobreNosAdapter adapter = new SobreNosAdapter(MainActivity_SobreNos.this, lista);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(layoutManager);

        /*final int duration = 20;
        final int pixelsToMove = 13;
        final Handler mHandler = new Handler(Looper.getMainLooper());
        final Runnable SCROLLING_RUNNABLE = new Runnable() {

            @Override
            public void run() {
                recyclerView.smoothScrollBy(pixelsToMove, 0);
                mHandler.postDelayed(this, duration);
            }
        };

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(final RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastItem = layoutManager.findLastCompletelyVisibleItemPosition();
                if(lastItem == layoutManager.getItemCount()-1){
                    mHandler.removeCallbacks(SCROLLING_RUNNABLE);
                    Handler postHandler = new Handler();
                    postHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            recyclerView.setAdapter(null);
                            recyclerView.setAdapter(adapter);
                            mHandler.postDelayed(SCROLLING_RUNNABLE, 2000);
                        }
                    }, 2000);
                }
            }
        });
        mHandler.postDelayed(SCROLLING_RUNNABLE, 2000);*/

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public String getVersionName() {
        String v = "";
        try {
            v = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return v;
    }
}



